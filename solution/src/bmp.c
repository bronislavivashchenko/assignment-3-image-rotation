#include "bmp.h"
#include <memory.h>

#define BMP_SIGNATURE       0x4D42
#define INFO_HEADER         40
#define BMP_PLANES          1
#define BITS_PER_PIXEL      24
#define COMPRESSION         0
#define IMPORTANT_COLORS    0
#define PADDING             4

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t calculate_padding(uint32_t width) {
    return (uint8_t) (PADDING - ((width * sizeof(struct pixel)) % PADDING)) % PADDING;
}

static enum read_status validate_header( const struct bmp_header header ) {
    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != BITS_PER_PIXEL) {
        return READ_INVALID_BITS;
    }
    if (header.biPlanes != BMP_PLANES ||
        header.biCompression != COMPRESSION ||
        header.biClrImportant != IMPORTANT_COLORS ||
        header.bOffBits != sizeof(struct bmp_header))
    {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (!in) {
        return READ_FILE_ERROR;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_FILE_ERROR;
    }

    enum read_status header_validation = validate_header(header);
    if (header_validation != READ_OK) {
        return header_validation;
    }

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;

    enum image_status image_created_status = image_create(img, width, height);
    if (image_created_status != IMAGE_OK) {
        return READ_IMAGE_CREATION_ERROR;
    }

    int padding = calculate_padding(width);

    if (fseek(in, (long) header.bOffBits, SEEK_SET)) {
        return READ_FILE_ERROR;
    }

    for (uint64_t i = 0; i < height; i++) {
        if (fread(img->data + i * width, sizeof(struct pixel) * width, 1, in) != 1) {
            return READ_FILE_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR)) {
            return READ_FILE_ERROR;
        }
    }

    return READ_OK;
}

static struct bmp_header generate_header(struct image const* img) {
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * img->width + calculate_padding(img->width)) * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = INFO_HEADER,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = COMPRESSION,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = IMPORTANT_COLORS,
    };
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!out) {
        return WRITE_FILE_ERROR;
    }

    struct bmp_header header = generate_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_FILE_ERROR;
    }

    uint8_t padding = calculate_padding(img->width);
    uint8_t padding_bytes[padding];
    memset(padding_bytes, 0, sizeof(uint8_t) * padding);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_FILE_ERROR;
        }
        if (fwrite(padding_bytes, 1, padding, out) != padding) {
            return WRITE_FILE_ERROR;
        }
    }

    return WRITE_OK;
}
