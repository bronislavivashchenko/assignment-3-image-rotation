#include "rotate.h"
#include <stdbool.h>

typedef uint64_t index(int64_t i, int64_t j, struct image const* img);

static bool validate_angle(int angle) {
    return angle >= ROTATE_MIN_ANGLE && angle <= ROTATE_MAX_ANGLE && angle % ROTATE_BASE_ANGLE == 0;
}

static uint64_t index_0degrees(int64_t i, int64_t j, struct image const* img) {
    return i * img->width + j;
}

static uint64_t index_90degrees(int64_t i, int64_t j, struct image const* img) {
    return j * img->width + img->width - i - 1;
}

static uint64_t index_180degrees(int64_t i, int64_t j, struct image const* img) {
    return (img->height - i - 1) * img->width + img->width - j - 1;
}

static uint64_t index_270degrees(int64_t i, int64_t j, struct image const* img) {
    return (img->height - j - 1) * img->width + i;
}

static void image_rotate_by_indexes(struct image* destination, struct image const* source, index to, index from) {
    for (int64_t i = 0; i < destination->height; i++) {
        for (int64_t j = 0; j < destination->width; j++) {
            destination->data[to(i, j, destination)] = source->data[from(i, j, source)];
        }
    }
}

enum rotate_status rotate( struct image* result, struct image const* source, int angle ) {
    if (!validate_angle(angle)) {
        return ROTATE_INVALID_ANGLE;
    }
    if (angle < 0) {
        angle += 360;
    }

    if ((angle == 0 || angle == 180) && (result->width != source->width || result->height != source->height)) {
        return ROTATE_INVALID_DESTINATION_IMAGE;
    }
    if ((angle == 90 || angle == 270) && (result->width != source->height || result->height != source->width)) {
        return ROTATE_INVALID_DESTINATION_IMAGE;
    }

    switch (angle) {
        case 0:
            image_rotate_by_indexes(result, source, index_0degrees, index_0degrees);
            break;
        case 90:
            image_rotate_by_indexes(result, source, index_0degrees, index_90degrees);
            break;
        case 180:
            image_rotate_by_indexes(result, source, index_0degrees, index_180degrees);
            break;
        case 270:
            image_rotate_by_indexes(result, source, index_0degrees, index_270degrees);
            break;
        default:
            return ROTATE_INVALID_ANGLE;
    }
    return ROTATE_OK;
}
