#include "bmp.h"
#include "rotate.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

const uint8_t EXIT_INVALID_ARGUMENTS = 1;
const uint8_t EXIT_INPUT_FILE_ERROR = 2;
const uint8_t EXIT_OUTPUT_FILE_ERROR = 3;
const uint8_t EXIT_BMP_READING_ERROR = 4;
const uint8_t EXIT_IMAGE_ROTATION_ERROR = 5;
const uint8_t EXIT_BMP_WRITING_ERROR = 6;

const char* const FILE_READ_BYTES = "rb";
const char* const FILE_WRITE_BYTES = "wb";

const char* const EXIT_INVALID_ANGLE_MESSAGE = "Invalid angle submitted. Available angles: 0, 90, -90, 180, -180, 270, -270\n";

int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(stderr, "Usage:\n%s <source-image> <transformed-image> <angle>\n", argv[0]);
        return EXIT_INVALID_ARGUMENTS;
    }

    char* input_filename = argv[1];
    char* output_filename = argv[2];
    int angle = atoi(argv[3]);

    FILE* input_file = fopen(input_filename, FILE_READ_BYTES);
    if (input_file == NULL) {
        fprintf(stderr, "Failed to open %s", input_filename);
        return EXIT_INPUT_FILE_ERROR;
    }

    FILE* output_file = fopen(output_filename, FILE_WRITE_BYTES);
    if (output_file == NULL) {
        fprintf(stderr, "Failed to open %s", output_filename);
        return EXIT_OUTPUT_FILE_ERROR;
    }

    struct image img = (struct image) {0, 0, NULL};
    enum read_status bmp_read_status = from_bmp(input_file, &img);
    if (bmp_read_status != READ_OK) {
        fprintf(stderr, "%s", read_status_messages[bmp_read_status]);
        return EXIT_BMP_READING_ERROR;
    }

    struct image img_new = (struct image) {0, 0, NULL};
    if (angle == 0 || angle == 180 || angle == -180) {
        image_create(&img_new, img.width, img.height);
    }
    else if (angle == 90 || angle == -90 || angle == 270 || angle == -270) {
        image_create(&img_new, img.height, img.width);
    }
    else {
        fprintf(stderr, "%s", EXIT_INVALID_ANGLE_MESSAGE);
        return EXIT_INVALID_ARGUMENTS;
    }

    enum rotate_status rotated_status = rotate(&img_new, &img, angle);
    if (rotated_status != ROTATE_OK) {
        fprintf(stderr, "%s", rotate_status_messages[rotated_status]);
        return EXIT_IMAGE_ROTATION_ERROR;
    }

    enum write_status bmp_written_status = to_bmp(output_file, &img_new);
    if (bmp_written_status != WRITE_OK) {
        fprintf(stderr, "%s", write_status_messages[bmp_written_status]);
        return EXIT_BMP_WRITING_ERROR;
    }

    fclose(input_file);
    fclose(output_file);

    free(img.data);
    free(img_new.data);

    return EXIT_SUCCESS;
}
