#include "image.h"
#include <malloc.h>
#include <memory.h>

enum image_status image_create( struct image* img, uint64_t width, uint64_t height ) {
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * width * height);

    if (!img->data) {
        return IMAGE_FAIL;
    }
    return IMAGE_OK;
}

enum image_status image_copy( struct image* destination, struct image const* source ) {
    enum image_status image_created = image_create(destination, source->width, source->height);
    if (image_created != IMAGE_OK) {
        return IMAGE_FAIL;
    }

    memcpy(destination->data, source->data, sizeof(struct pixel) * source->width * source->height);
    return IMAGE_OK;
}
