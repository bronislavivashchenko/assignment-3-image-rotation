#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_ERROR,
    READ_IMAGE_CREATION_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_FILE_ERROR
};

static const char* const read_status_messages[] = {
        [READ_OK] = "BMP read successfully",
        [READ_INVALID_SIGNATURE] = "BMP invalid signature",
        [READ_INVALID_BITS] = "BMP unsupported bits per pixel value",
        [READ_INVALID_HEADER] = "BMP corrupted header",
        [READ_FILE_ERROR] = "BMP file reading error",
        [READ_IMAGE_CREATION_ERROR] = "BMP image initialization error"
};

static const char* const write_status_messages[] = {
        [WRITE_OK] = "BMP written successfully",
        [WRITE_FILE_ERROR] = "BMP file writing error"
};

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_H
