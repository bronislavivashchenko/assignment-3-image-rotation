#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

enum image_status {
    IMAGE_OK = 0,
    IMAGE_FAIL
};

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

enum image_status image_create( struct image* img, uint64_t width, uint64_t height );

enum image_status image_copy( struct image* destination, struct image const* source );

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
