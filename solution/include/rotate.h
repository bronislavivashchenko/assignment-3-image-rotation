#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H

#include "image.h"

#define ROTATE_BASE_ANGLE 90
#define ROTATE_MIN_ANGLE (-270)
#define ROTATE_MAX_ANGLE 270

enum rotate_status {
    ROTATE_OK,
    ROTATE_INVALID_ANGLE,
    ROTATE_IMAGE_CREATION_ERROR,
    ROTATE_INVALID_DESTINATION_IMAGE
};

static const char* const rotate_status_messages[] = {
        [ROTATE_OK] = "Rotated successfully",
        [ROTATE_INVALID_ANGLE] = "Invalid rotation angle. Available angles: 0, 90, -90, 180, -180, 270, -270",
        [ROTATE_IMAGE_CREATION_ERROR] = "Failed to allocate image for rotation",
        [ROTATE_INVALID_DESTINATION_IMAGE] = "Invalid rotation destination image"
};

enum rotate_status rotate( struct image* result, struct image const* source, int angle );

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
